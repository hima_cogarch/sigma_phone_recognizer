//
//  sigma_utils.h
//  This file contains defines/structs required to generate Sigma code.
//  ASR Utils
//
//  Created by Himanshu Joshi on 1/11/16.
//  Copyright © 2016 Take Infinity. All rights reserved.
//

#ifndef sigma_utils_h
#define sigma_utils_h
#include "common_includes.h"
#include <stdio.h>

bool generate_sigma_transition_factor(FILE* out_file);


#endif /* sigma_utils_h */
