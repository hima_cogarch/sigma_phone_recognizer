//
//  timit_defines.h
//  ASR Utils
//
//  Created by Himanshu Joshi on 1/11/16.
//  Copyright © 2016 Take Infinity. All rights reserved.
//

#ifndef timit_defines_h
#define timit_defines_h

//This is a giant hack, can be read from the console using a small for loop
extern int timit_defines_num_timit_phones;

/*  Parameters related to the acoustic front end */
#define SIGMA_TIMIT_ACOUSTIC_LOWER_BOUND_ON_OBS (-43.00)
#define SIGMA_TIMIT_ACOUSTIC_UPPER_BOUND_ON_OBS (43.00)

#endif /* timit_defines_h */
