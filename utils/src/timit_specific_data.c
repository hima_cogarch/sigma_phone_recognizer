//
//  timit_specific_data.c
//  ASR Utils
//
//  Created by Himanshu Joshi on 1/26/16.
//  Copyright © 2016 Take Infinity. All rights reserved.
//

#include "timit_defines.h"

const char* timit_train_phone_list[] = {
  "sil",
  "aa",
  "ae",
  "ah",
  "ao",
  "aw",
  "ax",
  "ay",
  "b",
  "ch",
  "cl",
  "d",
  "dh",
  "dx",
  "eh",
  "el",
  "en",
  "epi",
  "er",
  "ey",
  "f",
  "g",
  "hh",
  "ih",
  "ix",
  "iy",
  "jh",
  "k",
  "l",
  "m",
  "n",
  "ng",
  "ow",
  "oy",
  "p",
  "q",
  "r",
  "s",
  "sh",
  "t",
  "th",
  "uh",
  "uw",
  "v",
  "vcl",
  "w",
  "y",
  "z",
  "zh"
};

int timit_defines_num_timit_phones = sizeof(timit_train_phone_list)/sizeof(timit_train_phone_list[0]);
