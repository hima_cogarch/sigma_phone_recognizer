//
//  sigma_asr_generator.c
//  ASR Utils
//
//  Created by Himanshu Joshi on 1/25/16.
//  Copyright © 2016 Take Infinity. All rights reserved.
//

#include "sigma_asr_generator.h"
#include "common_includes.h"
#include "sigma_conditional.h"
#include "timit_defines.h"
#include "stdlib.h"
#include "stdio.h"

//for some reason the memcpy in string . h is referenced. Add this to get rid of the dumb warning
#include "string.h"

//All the miscellaneous const strings that can be used to configure the template code.
typedef struct{
  char*  uniform_function_name;
  char*  trans_fn_name_root;  
  char*  state_name_root;  
  char*  state_sel_name;  
  char*  state_sel_prev_name;
  char*  observation_pred_root;
  char*  observation_fn_root;
  char*  phone_transition_name;
  char*  state_copy_pred_name;
}sigma_asr_gen_misc_strings_s;
//For now cram all the acoustic front end and HMM/DBN generation in one file.

static sigma_asr_gen_misc_strings_s asr_strings =
{
  
    .uniform_function_name = "\'( (1 *))",
    .phone_transition_name = "phone_transition_t",
    .trans_fn_name_root    = "state_trans_fn",
    .state_name_root       = "phone_state_ts",
    .state_sel_name        = "phone_state_sel",
    .state_sel_prev_name   = "phone_state_sel_prev",
    .observation_pred_root = "observation",            //observation_dimN_ts_T
    .observation_fn_root   = "obs_fn",                 //obs_fn_dimN_ts_T
    .state_copy_pred_name  = "state_copy_pred_ts",
};

sigma_predicate_s forward_constraint_enforcer  =
{
  .predicate_name = "fwd_constraint_enforcer",
  .predicate_specific_learning_rate = true,
  .learning_rate  = 0.0,
  .num_dims       = 2,
  .predicate_dim = {
    {.type_name = "boolean",    .var_name = "happened",  .selection_type = SIGMA_PRED_CLOSED_NONE },
    {.type_name = "sfsa-state", .var_name = "sfsa-posn", .selection_type = SIGMA_PRED_CLOSED_MAINTAIN_ALL },
  },
  .has_function  = true,
  .function_name = "*trans-fun-forward-constaint*",
};

sigma_predicate_s backward_constraint_enforcer =
{
  .predicate_name = "backward_constraint_enforcer",
  .predicate_specific_learning_rate = true,
  .learning_rate  = 0.0,
  .num_dims       = 2,
  .predicate_dim = {
    {.type_name = "boolean",    .var_name = "happened",  .selection_type = SIGMA_PRED_CLOSED_NONE },
    {.type_name = "sfsa-state", .var_name = "sfsa-posn", .selection_type = SIGMA_PRED_CLOSED_MAINTAIN_ALL },
  },
  .has_function  = true,
  .function_name = "*trans-fun-backward-constaint*",
};

sigma_predicate_s phone_state_prior_predicate =
{
  .predicate_name = "phone_state_prior_predicate",
  .world          = SIGMA_PRED_OPEN_WORLD,
  .is_perceptual  = true,
  .replace        = SIGMA_PRED_REPLACE,
  .predicate_specific_learning_rate = false,
  .learning_rate  = 0.0,
  .num_dims       = 2,
  .predicate_dim = {
    {.type_name = "phone",    .var_name = "phone",  .selection_type = SIGMA_PRED_CLOSED_MAINTAIN_ALL },
    {.type_name = "sfsa-state", .var_name = "sfsa-posn", .selection_type = SIGMA_PRED_CLOSED_MAINTAIN_ALL },
  },
  .has_function  = false,
  .function_name = "", // "*trans-fun-backward-constaint*",
};

sigma_predicate_s phone_state_copy_to_predicate =
{
  .predicate_name = "phone_state_prior_predicate",
  .world          = SIGMA_PRED_CLOSED_WORLD,
  .is_perceptual  = false,
  .replace        = SIGMA_PRED_NO_REPLACE,
  .predicate_specific_learning_rate = false,
  .learning_rate  = 0.0,
  .num_dims       = 2,
  .predicate_dim = {
    {.type_name = "phone",    .var_name = "phone",  .selection_type = SIGMA_PRED_CLOSED_MAINTAIN_ALL },
    {.type_name = "sfsa-state", .var_name = "sfsa-posn", .selection_type = SIGMA_PRED_CLOSED_MAINTAIN_ALL },
  },
  .has_function  = false,
  .function_name = "", // "*trans-fun-backward-constaint*",
};

sigma_predicate_s phone_state_sel_from_most_recent =
{
  .predicate_name = "phone_state_sel",
  .world          = SIGMA_PRED_CLOSED_WORLD,
  .is_perceptual  = false,
  .replace        = SIGMA_PRED_NO_REPLACE,
  .predicate_specific_learning_rate = true,
  .learning_rate  = 0.0,
  .num_dims       = 2,
  .predicate_dim = {
    {.type_name = "phone",    .var_name = "phone",  .selection_type = SIGMA_PRED_CLOSED_BEST_SEL },
    {.type_name = "sfsa-state", .var_name = "sfsa-posn", .selection_type = SIGMA_PRED_CLOSED_BEST_SEL },
  },
  .has_function  = false,
  .function_name = "", // "*trans-fun-backward-constaint*",
};

sigma_predicate_s phone_state_sel_from_most_recent_prev =
{
  .predicate_name = "phone_state_sel_prev",
  .world          = SIGMA_PRED_CLOSED_WORLD,
  .is_perceptual  = false,
  .replace        = SIGMA_PRED_NO_REPLACE,
  .predicate_specific_learning_rate = true,
  .learning_rate  = 0.0,
  .num_dims       = 2,
  .predicate_dim = {
    {.type_name = "phone",    .var_name = "phone",  .selection_type = SIGMA_PRED_CLOSED_BEST_SEL },
    {.type_name = "sfsa-state", .var_name = "sfsa-posn", .selection_type = SIGMA_PRED_CLOSED_BEST_SEL },
  },
  .has_function  = false,
  .function_name = "", // "*trans-fun-backward-constaint*",
};

sigma_predicate_s state_transition_fn_predicate_template =
{
  .predicate_name = "",
  .world          = SIGMA_PRED_OPEN_WORLD,
  .is_perceptual  = false,
  .replace        = SIGMA_PRED_NO_REPLACE,
  .predicate_specific_learning_rate = false,
  .learning_rate  = 0.0,
  .num_dims       = 4,
  .predicate_dim = {
    {.type_name = "phone",    .var_name = "phone0",  .selection_type = SIGMA_PRED_CLOSED_NONE },
    {.type_name = "sfsa-state", .var_name = "sfsa-posn0", .selection_type = SIGMA_PRED_CLOSED_NONE },
    {.type_name = "phone",    .var_name = "phone1",  .selection_type = SIGMA_PRED_CLOSED_MAINTAIN_ALL },
    {.type_name = "sfsa-state", .var_name = "sfsa-posn1", .selection_type = SIGMA_PRED_CLOSED_MAINTAIN_ALL },
  },
  .has_function  = true,
  .function_name = "*global-phone-trans-fn*", // "*trans-fun-backward-constaint*",
};

sigma_predicate_s phone_transition_predicate_template  =
{
  .predicate_name = "",
  .world          = SIGMA_PRED_OPEN_WORLD,
  .is_perceptual  = true,
  .replace        = SIGMA_PRED_NO_REPLACE,
  .predicate_specific_learning_rate = false,
  .learning_rate  = 0.0,
  .num_dims       = 1,
  .predicate_dim = {
    {.type_name = "boolean",    .var_name = "happened",  .selection_type = SIGMA_PRED_CLOSED_MAINTAIN_ALL },
  },
  .has_function  = false,
  .function_name = "", // "*trans-fun-backward-constaint*",
};

//(PREDICATE 'OBSERVATION1-T :WORLD 'OPEN :UNIQUE '(OBSERVATION) :PERCEPTION T :ARGUMENTS '((OBSERVATION OBSERVATION %)))
//observation_dimN_ts_T
sigma_predicate_s observation_predicate_template  =
{
  .predicate_name = "",
  .world          = SIGMA_PRED_OPEN_WORLD,
  .is_perceptual  = true,
  .replace        = SIGMA_PRED_NO_REPLACE,
  .predicate_specific_learning_rate = false,
  .learning_rate  = 0.0,
  .num_dims       = 1,
  .predicate_dim = {
    {.type_name = "observation",    .var_name = "obs",  .selection_type = SIGMA_PRED_CLOSED_MAINTAIN_ALL },
  },
  .has_function  = false,
  .function_name = "", // "*trans-fun-backward-constaint*",
};

//(PREDICATE 'OBS-FN-TS1-DIM1 :WORLD 'CLOSED :PERSISTENT T :UNIQUE '(OBSERVATION) :ARGUMENTS '((PHONE PHONE) (SFSA-POSN SFSA-STATE) (OBSERVATION OBSERVATION %)) :FUNCTION '((1 *)))
//obs_fn_dimN_ts_T
sigma_predicate_s observation_fn_predicate_template  =
{
  .predicate_name = "",
  .world          = SIGMA_PRED_CLOSED_WORLD,
  .is_perceptual  = true,
  .replace        = SIGMA_PRED_NO_REPLACE,
  .predicate_specific_learning_rate = false,
  .learning_rate  = 0.0,
  .num_dims       = 2,
  .predicate_dim = {
    {.type_name = "sfsa-state",    .var_name = "sfsa-posn",  .selection_type = SIGMA_PRED_CLOSED_NONE},
    {.type_name = "observation",    .var_name = "obs",  .selection_type = SIGMA_PRED_CLOSED_MAINTAIN_ALL },
  },
  .has_function  = true,
  .function_name = "", //asr_strings.uniform_function_name, // "*trans-fun-backward-constaint*",
};


sigma_conditional_s copy_from_most_recent_ts =
{
  .conditional_name = "state_copy_from_most_recent_ts",
  .num_conditions   = 1,
  .num_actions      = 1,
  .num_condacts     = 0,
  .condition_patterns = 
  { },
  .actions_patterns    =
  { },
  .condacts_patterns  = 
  { },
};
sigma_conditional_s copy_from_most_recent_ts_prev =
{
  .conditional_name = "state_copy_from_most_recent_ts_prev",
  .num_conditions   = 1,
  .num_actions      = 1,
  .num_condacts     = 0,
  .condition_patterns = 
  { },
  .actions_patterns    =
  { },
  .condacts_patterns  = 
  { },
};

sigma_conditional_s phone_trans_detector =
{
  .conditional_name = "detect_phone_transition",
  .num_conditions   = 2,
  .num_actions      = 1,
  .num_condacts     = 0,
  .condition_patterns = 
  { },
  .actions_patterns    =
  { },
  .condacts_patterns  = 
  { },
};




#define MAX_OBS_TIME_SLICES   16
#define NUM_OBS_DIMS          39

typedef struct{
  int                 num_time_slices_to_generate;
  sigma_predicate_s*  sigma_asr_generator_phone_state_preds[MAX_OBS_TIME_SLICES];
  sigma_predicate_s*  sigma_asr_generator_trans_fn_preds[MAX_OBS_TIME_SLICES];
  sigma_predicate_s*  sigma_asr_generator_ph_trans_preds[MAX_OBS_TIME_SLICES];
  sigma_predicate_s*  sigma_asr_generator_copy_state_preds[MAX_OBS_TIME_SLICES];
  sigma_predicate_s*  sigma_asr_generator_observation_preds[MAX_OBS_TIME_SLICES][NUM_OBS_DIMS];
  sigma_predicate_s*  sigma_asr_generator_obs_fn_preds[MAX_OBS_TIME_SLICES][NUM_OBS_DIMS];

  sigma_conditional_s* sigma_asr_generator_copy_to_prior_conditionals[MAX_OBS_TIME_SLICES]; //Need two per time slice.
  sigma_conditional_s* sigma_asr_generator_copy_from_prior_conditionals[MAX_OBS_TIME_SLICES]; //Need two per time slice.

  //One per time slice
  sigma_conditional_s* sigma_asr_generator_combinator_conditionals[MAX_OBS_TIME_SLICES];

  //One per time slice per dimension
  sigma_conditional_s* sigma_asr_generator_obs_conditionals[MAX_OBS_TIME_SLICES][NUM_OBS_DIMS];

}sigma_asr_generator_config_s;


static sigma_asr_generator_config_s asr_config =
{
  .num_time_slices_to_generate = 2,
  //rest of the variables *SHOULD* be NULL since this data is going to a ZI page
};

void sigma_asr_generator_init
(
  void
)
{

  //Do any initiatlizations that must be hacked //TODO: this has to be done by the compiler at compile time
  memcpy(observation_fn_predicate_template.function_name,asr_strings.uniform_function_name,MAX_NAME_LEN);

  //Allocate all memory here...
  int total_mem_size = asr_config.num_time_slices_to_generate * 
                        ( (sizeof(sigma_predicate_s) * 4 ) + ( 2* (sizeof(sigma_predicate_s)) * NUM_OBS_DIMS));


  //TODO: the following loops can be merged.
  sigma_predicate_s* mem_base = (sigma_predicate_s*)malloc(total_mem_size);
  memset(mem_base,0x0,total_mem_size);
  
  for(int slice =0;slice < asr_config.num_time_slices_to_generate; slice++)
  {
    asr_config.sigma_asr_generator_phone_state_preds[slice] = mem_base++;
  }
  for(int slice =0;slice < asr_config.num_time_slices_to_generate; slice++)
  {
    asr_config.sigma_asr_generator_trans_fn_preds[slice] = mem_base++;
  }
  for(int slice =0;slice < asr_config.num_time_slices_to_generate; slice++)
  {
    asr_config.sigma_asr_generator_ph_trans_preds[slice] = mem_base++;
  }
  for(int slice =0;slice < asr_config.num_time_slices_to_generate; slice++)
  {
    asr_config.sigma_asr_generator_copy_state_preds[slice] = mem_base++;
  }
  //Now the observation predicate
  for(int slice =0;slice < asr_config.num_time_slices_to_generate; slice++)
  {
    for(int dim =0; dim < NUM_OBS_DIMS; dim++)
    {
      asr_config.sigma_asr_generator_observation_preds[slice][dim] = mem_base++;
    }
  }
  //Now the observation function predicates
  for(int slice =0;slice < asr_config.num_time_slices_to_generate; slice++)
  {
    for(int dim =0; dim < NUM_OBS_DIMS; dim++)
    {
      asr_config.sigma_asr_generator_obs_fn_preds[slice][dim] = mem_base++;
    }
  }

  for(int slice =0;slice < asr_config.num_time_slices_to_generate; slice++)
  {
    asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice] =
      (sigma_conditional_s*) malloc(sizeof(sigma_conditional_s));
  }
  for(int slice =0;slice < asr_config.num_time_slices_to_generate; slice++)
  {
    asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice] =
      (sigma_conditional_s*) malloc(sizeof(sigma_conditional_s));
  }

  for(int slice =0;slice < asr_config.num_time_slices_to_generate; slice++)
  {
    asr_config.sigma_asr_generator_combinator_conditionals[slice] =
      (sigma_conditional_s*) malloc(sizeof(sigma_conditional_s));
  }

  for (int slice =0; slice < asr_config.num_time_slices_to_generate; slice++)
  {
    for(int dim =0; dim < NUM_OBS_DIMS; dim++)
    {
      asr_config.sigma_asr_generator_obs_conditionals[slice][dim] =
        (sigma_conditional_s*) malloc(sizeof(sigma_conditional_s));
    }
  }

  return;
}//init

void sigma_asr_generator_fill_state_pred
(
 sigma_predicate_s            * pred,
 sigma_asr_generator_config_s * asr_config
)
{
  //Use the prior predicate defn as a template
  memcpy(pred,&phone_state_prior_predicate,sizeof(sigma_predicate_s));

}/*sigma_asr_generator_fill_state_pred */

void sigma_asr_generator_generate_state_trans_predicates
(
  sigma_asr_generator_config_s* asr_config
)
{
  int this_slice = 0;
  int num_slices = asr_config->num_time_slices_to_generate;
  sigma_predicate_s* this_state_pred       = NULL;
  sigma_predicate_s* this_trans_fn_pred    = NULL;
  sigma_predicate_s* this_phone_trans_pred = NULL;
  sigma_predicate_s* this_prior_pred       = NULL;

  do{

    if(asr_config == NULL) break;


    for(this_slice = 0; this_slice < num_slices; this_slice++)
    {
      this_state_pred       = asr_config->sigma_asr_generator_phone_state_preds[this_slice];
      this_trans_fn_pred    = asr_config->sigma_asr_generator_trans_fn_preds[this_slice];
      this_phone_trans_pred = asr_config->sigma_asr_generator_ph_trans_preds[this_slice];
      this_prior_pred       = asr_config->sigma_asr_generator_copy_state_preds[this_slice];

      //Now assign each one a name etc.
      memcpy(this_state_pred,&phone_state_prior_predicate,sizeof(sigma_predicate_s));
      snprintf(this_state_pred->predicate_name, MAX_NAME_LEN, "%s_%d",asr_strings.state_name_root,this_slice);

      memcpy(this_trans_fn_pred,&state_transition_fn_predicate_template,sizeof(sigma_predicate_s));
      snprintf(this_trans_fn_pred->predicate_name, MAX_NAME_LEN, "%s_%d",asr_strings.trans_fn_name_root,this_slice);

      if (this_slice != 0)
      {
        snprintf(this_trans_fn_pred->predicate_name, MAX_NAME_LEN, "%s_%d",asr_strings.trans_fn_name_root,this_slice);
        snprintf(this_trans_fn_pred->function_name, MAX_NAME_LEN, "'%s",asr_config->sigma_asr_generator_trans_fn_preds[0]->predicate_name);
      }

      memcpy(this_phone_trans_pred,&phone_transition_predicate_template,sizeof(sigma_predicate_s));
      snprintf(this_phone_trans_pred->predicate_name, MAX_NAME_LEN, "%s_%d",asr_strings.phone_transition_name,this_slice);

      memcpy(this_prior_pred, &phone_state_copy_to_predicate, sizeof(sigma_predicate_s));
      snprintf(this_prior_pred->predicate_name, MAX_NAME_LEN, "%s_%d",asr_strings.state_copy_pred_name,this_slice);
    }
  }while(0);

  return;
}/*  sigma_asr_generator_generate_state_trans_predicates */

void sigma_asr_generator_generate_obs_obs_fn_predicates
(
  sigma_asr_generator_config_s* asr_config
)
{
  int slice = 0;
  int num_slices = asr_config->num_time_slices_to_generate;
  sigma_predicate_s * pred_to_make = NULL;

  do{

    if(asr_config == NULL) break;

    //observation_dimN_ts_T
    for(slice =0;slice < num_slices; slice++)
    {
      for(int dim =0; dim < NUM_OBS_DIMS; dim++)
      {
        pred_to_make = asr_config->sigma_asr_generator_observation_preds[slice][dim];
        memcpy(pred_to_make,&observation_predicate_template,sizeof(sigma_predicate_s));
        snprintf(pred_to_make->predicate_name, MAX_NAME_LEN,"%s%s%d%s%d",
                 asr_strings.observation_pred_root, "_dim",dim,"_ts_",slice);
      }
    }
    //Now the observation function predicates: obs_fn_dimN_ts_T
    for(slice =0;slice < asr_config->num_time_slices_to_generate; slice++)
    {
      for(int dim =0; dim < NUM_OBS_DIMS; dim++)
      {
        pred_to_make = asr_config->sigma_asr_generator_obs_fn_preds[slice][dim];
        memcpy(pred_to_make,&observation_fn_predicate_template,sizeof(sigma_predicate_s));
        snprintf(pred_to_make->predicate_name, MAX_NAME_LEN,"%s%s%d%s%d",
                 asr_strings.observation_fn_root, "_dim",dim,"_ts_",slice);
        if(slice>0)
          snprintf(pred_to_make->function_name, MAX_NAME_LEN, "'%s",
                 asr_config->sigma_asr_generator_obs_fn_preds[0][dim]->predicate_name);

      }
    }
    //Now write the name of the first slice's obs function


  }while(0);

  return;
}/*  sigma_asr_generator_generate_obs_obs_fn_predicates */

void sigma_asr_generator_make_predicates
(
  void
)
{

  //Now generate the state and transition function, phone transition predicates
  sigma_asr_generator_generate_state_trans_predicates(&asr_config);
  sigma_asr_generator_generate_obs_obs_fn_predicates(&asr_config);

}/* sigma_asr_generator_make_predicates */

void sigma_asr_generator_print_asr_predicates
(
  FILE * out_stream
)
{

  do{

    if(out_stream == NULL)
    {
      printf("No stream to print asr configuration to, bailing out \n");
      break;
    }
    //check for invalid configuration here.... //TODO

    //Print the constraint enforcers now
    print_predicate_declaration_to_stream(out_stream,&backward_constraint_enforcer);
    print_predicate_declaration_to_stream(out_stream,&forward_constraint_enforcer);

    //Now get to the meat of the recognizer
    print_predicate_declaration_to_stream(out_stream,&phone_state_prior_predicate);

    //Now print all the predicates
    for (int slice =0; slice < asr_config.num_time_slices_to_generate; slice++)
    {
      print_predicate_declaration_to_stream(out_stream, asr_config.sigma_asr_generator_phone_state_preds[slice]);
      print_predicate_declaration_to_stream(out_stream, asr_config.sigma_asr_generator_trans_fn_preds[slice]);
      print_predicate_declaration_to_stream(out_stream, asr_config.sigma_asr_generator_ph_trans_preds[slice]);
      print_predicate_declaration_to_stream(out_stream, asr_config.sigma_asr_generator_copy_state_preds[slice]);
    }

    for (int slice=0; slice < asr_config.num_time_slices_to_generate; slice++) {
      for (int dim =0 ; dim< NUM_OBS_DIMS; dim++) {
        print_predicate_declaration_to_stream(out_stream, asr_config.sigma_asr_generator_observation_preds[slice][dim]);
      }
    }

    for (int slice=0; slice < asr_config.num_time_slices_to_generate; slice++) {
      for (int dim =0 ; dim< NUM_OBS_DIMS; dim++) {
        print_predicate_declaration_to_stream(out_stream, asr_config.sigma_asr_generator_obs_fn_preds[slice][dim]);
      }
    }


  }while(0);

}


void sigma_asr_generator_make_conditionals
(
  void
)
{
  int slices = asr_config.num_time_slices_to_generate;
  int slice = 0;

  do{
    //Make the state selection conditionals first.
    if(slices < 2)
    {
      printf("Cannot generate conditionals for less than 2 timeslices: %d",asr_config.num_time_slices_to_generate);
      break;
    }

    //Do the easy conditionals first.
    copy_from_most_recent_ts.num_actions = 1;
    copy_from_most_recent_ts.num_conditions = 1;

    copy_from_most_recent_ts_prev.num_actions = 1;
    copy_from_most_recent_ts_prev.num_conditions = 1;
    copy_from_most_recent_ts.actions_patterns[0] = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));
    copy_from_most_recent_ts.condition_patterns[0] = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));
    copy_from_most_recent_ts_prev.actions_patterns[0] = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));
    copy_from_most_recent_ts_prev.condition_patterns[0] = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));

    convert_predicate_decl_2_predicate_pattern(asr_config.sigma_asr_generator_phone_state_preds[slices-1],
                                               copy_from_most_recent_ts.condition_patterns[0]);
    convert_predicate_decl_2_predicate_pattern(&phone_state_sel_from_most_recent,
                                               copy_from_most_recent_ts.actions_patterns[0]);
    strncpy(copy_from_most_recent_ts.condition_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
    strncpy(copy_from_most_recent_ts.condition_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);
    strncpy(copy_from_most_recent_ts.actions_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
    strncpy(copy_from_most_recent_ts.actions_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);



    convert_predicate_decl_2_predicate_pattern(asr_config.sigma_asr_generator_phone_state_preds[slices-2],
                                               copy_from_most_recent_ts_prev.condition_patterns[0]);
    convert_predicate_decl_2_predicate_pattern(&phone_state_sel_from_most_recent_prev,
                                               copy_from_most_recent_ts_prev.actions_patterns[0]);

    strncpy(copy_from_most_recent_ts_prev.condition_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
    strncpy(copy_from_most_recent_ts_prev.condition_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);
    strncpy(copy_from_most_recent_ts_prev.actions_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
    strncpy(copy_from_most_recent_ts_prev.actions_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);


    //TODO: The phone-transition conditional.
#if 0
    (CONDITIONAL 'COPY-T-TO-PRIOR
     :CONDITIONS '((PHONE-STATE-OBS-COMBINATOR-T (PHONE (PHONE)) (SFSA-POSN (SFSA-POSN))))
     :ACTIONS '((PHONE-STATE-OBS-COMBINATOR-PRIOR (PHONE (PHONE)) (SFSA-POSN (SFSA-POSN))))
     )

    (CONDITIONAL 'COPY-T+1-TO-PRIOR
     :CONDITIONS '((PHONE-STATE-OBS-COMBINATOR-T+1 (PHONE (PHONE)) (SFSA-POSN (SFSA-POSN))))
     :ACTIONS '((PHONE-STATE-OBS-COMBINATOR-T-PRIOR (PHONE (PHONE)) (SFSA-POSN (SFSA-POSN))))
     )

    (CONDITIONAL 'COPY-FROM-T-PRIOR
     :CONDITIONS '((PHONE-STATE-OBS-COMBINATOR-PRIOR (PHONE (PHONE)) (SFSA-POSN (SFSA-POSN))))
     :ACTIONS '((PHONE-STATE-OBS-COMBINATOR-T-1 (PHONE (PHONE)) (SFSA-POSN (SFSA-POSN))))
     )
#endif

    //NOw the diachronic processing & acoutic modeling conditionals
    // start with the copy ones phone_state_ts_N -> state_copy_pred -> phone_state_ts_N-1
    for (slice = asr_config.num_time_slices_to_generate-1; slice > 0; slice--)
    {
      snprintf(asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->conditional_name,
               MAX_NAME_LEN,"phone_state_copy_to_%d",slice);
      asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->num_conditions = 1;
      asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->num_actions = 1;

      asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->condition_patterns[0]
        = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));
      asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->actions_patterns[0]
        = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));

      convert_predicate_decl_2_predicate_pattern(asr_config.sigma_asr_generator_phone_state_preds[slice]
        ,asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->condition_patterns[0]);
      convert_predicate_decl_2_predicate_pattern(asr_config.sigma_asr_generator_copy_state_preds[slice]
        ,asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->actions_patterns[0]);

      strncpy(asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->condition_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
      strncpy(asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->condition_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);
      strncpy(asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->actions_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
      strncpy(asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->actions_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);

      //asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->condition_patterns[0]->predicate_dim[0].val_var_name
      //    asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->condition_patterns[0]  = asr_config.sigma_asr_generator_phone_state_preds[slice];
      //    asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->actions_patterns[0]    = asr_config.sigma_asr_generator_copy_state_preds[slice];

      snprintf(asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->conditional_name,
               MAX_NAME_LEN,"phone_state_from_%d",slice);

      asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->num_actions = 1;
      asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->num_conditions = 1;

      asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->condition_patterns[0]
        = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));
      asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->actions_patterns[0]
        = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));

      convert_predicate_decl_2_predicate_pattern
        (asr_config.sigma_asr_generator_copy_state_preds[slice],
        asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->condition_patterns[0]);
      convert_predicate_decl_2_predicate_pattern
        (asr_config.sigma_asr_generator_phone_state_preds[slice-1],
         asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->actions_patterns[0]);


      //asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->condition_patterns[0]        = asr_config.sigma_asr_generator_copy_state_preds[slice];
      //asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->actions_patterns[0]        = asr_config.sigma_asr_generator_phone_state_preds[slice-1];

      strncpy(asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->condition_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
      strncpy(asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->condition_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);
      strncpy(asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->actions_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
      strncpy(asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->actions_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);
    }
    //We have stopped when slice is 0; so now do the copy from phone_state_ts0 -> phone_state_prior_predicate
    snprintf(asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->conditional_name,
             MAX_NAME_LEN,"phone_state_copy_to_%d",slice);

    asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->num_conditions = 1;
    asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->num_actions = 1;
    asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->actions_patterns[0]
      = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));

    asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->condition_patterns[0]
    = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));

    convert_predicate_decl_2_predicate_pattern(asr_config.sigma_asr_generator_phone_state_preds[slice],
                                               asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->condition_patterns[0]);
    convert_predicate_decl_2_predicate_pattern(asr_config.sigma_asr_generator_copy_state_preds[slice],
                                               asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->actions_patterns[0]);
    //asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->condition_patterns[0]      = asr_config.sigma_asr_generator_phone_state_preds[slice];
    //asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->actions_patterns[0]      = asr_config.sigma_asr_generator_copy_state_preds[slice];
    strncpy(asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->condition_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
    strncpy(asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->condition_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);
    strncpy(asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->actions_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
    strncpy(asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]->actions_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);


    snprintf(asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->conditional_name,
             MAX_NAME_LEN,"phone_state_from_%d",slice);
    asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->num_actions = 1;
    asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->num_conditions = 1;

    asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->actions_patterns[0]
      = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));
    asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->condition_patterns[0]
      = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));

    convert_predicate_decl_2_predicate_pattern(asr_config.sigma_asr_generator_copy_state_preds[slice],
                                               asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->condition_patterns[0]);
    convert_predicate_decl_2_predicate_pattern(&phone_state_prior_predicate,
                                               asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->actions_patterns[0]);
    //asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->condition_patterns[0]      = asr_config.sigma_asr_generator_copy_state_preds[slice];
    //asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->actions_patterns[0]      = &phone_state_prior_predicate;
    strncpy(asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->condition_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
    strncpy(asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->condition_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);
    strncpy(asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->actions_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
    strncpy(asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]->actions_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);


    //Now the acoustic observation conditionals
#if 0
    (conditional 'state-obs1-t+1
     :condacts '(
     (phone-state-obs-combinator-t+1 (phone (phone)) (sfsa-posn (sfsa-posn)))
     (obs-fn-ts2-dim1      ( phone (phone) ) (sfsa-posn (sfsa-posn)) (observation (observation)) )
     (observation1-t+1 (observation (observation)))
     )
    )
#endif
    //each conditional is 'state-ts_N_dim_M
    for (slice = 0; slice < slices; slice++) {
      for (int dim = 0; dim < NUM_OBS_DIMS; dim++) {
        sigma_conditional_s* this_cond = asr_config.sigma_asr_generator_obs_conditionals[slice][dim];
        snprintf(this_cond->conditional_name,
                 MAX_NAME_LEN,"state_obs_ts_%d_dim_%d",slice,dim);

        this_cond->num_condacts = 3;
        this_cond->num_conditions = 0;
        this_cond->num_actions = 0;
        this_cond->condacts_patterns[0] = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));
        this_cond->condacts_patterns[1] = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));
        this_cond->condacts_patterns[2] = (sigma_predicate_pattern_s*)malloc(sizeof(sigma_predicate_pattern_s));
        
        convert_predicate_decl_2_predicate_pattern(asr_config.sigma_asr_generator_phone_state_preds[slice],
                                                   this_cond->condacts_patterns[0]);

        convert_predicate_decl_2_predicate_pattern(asr_config.sigma_asr_generator_obs_fn_preds[slice][dim],
                                                   this_cond->condacts_patterns[1]);

        convert_predicate_decl_2_predicate_pattern(asr_config.sigma_asr_generator_observation_preds[slice][dim],
                                                   this_cond->condacts_patterns[2]);
        strncpy(this_cond->condacts_patterns[0]->predicate_dim[0].val_var_name,"phone0",MAX_NAME_LEN);
        strncpy(this_cond->condacts_patterns[0]->predicate_dim[1].val_var_name,"sfsa-posn0",MAX_NAME_LEN);
        strncpy(this_cond->condacts_patterns[1]->predicate_dim[0].val_var_name,"sfsa-posn0",MAX_NAME_LEN);
        strncpy(this_cond->condacts_patterns[1]->predicate_dim[1].val_var_name,"obs",MAX_NAME_LEN);
        strncpy(this_cond->condacts_patterns[2]->predicate_dim[0].val_var_name,"obs",MAX_NAME_LEN);

        //this_cond->condacts_patterns[0] = asr_config.sigma_asr_generator_phone_state_preds[slice];
        //this_cond->condacts_patterns[1] = asr_config.sigma_asr_generator_obs_fn_preds[slice][dim];
        //this_cond->condacts_patterns[2] = asr_config.sigma_asr_generator_observation_preds[slice][dim];
      }
    }
  }while(0);

  return;
}

void sigma_asr_generator_print_asr_conditionals
(
 FILE * out_stream
 )
{

  int slice = 0;
  int dim   = 0;
  
#if 0
  print_conditional_to_stream(out_stream,&copy_from_most_recent_ts);
  print_conditional_to_stream(out_stream,&copy_from_most_recent_ts_prev);

  //Now timeslice related conditionals
  for(slice =0; slice < asr_config.num_time_slices_to_generate;slice++)
  {
    print_conditional_to_stream(out_stream, asr_config.sigma_asr_generator_copy_to_prior_conditionals[slice]);
    print_conditional_to_stream(out_stream, asr_config.sigma_asr_generator_copy_from_prior_conditionals[slice]);
  }
#endif
  for (slice=0; slice < asr_config.num_time_slices_to_generate; slice++) {
    for (dim =0 ; dim< NUM_OBS_DIMS; dim++) {
      print_conditional_to_stream(out_stream, asr_config.sigma_asr_generator_obs_conditionals[slice][dim]);
    }
  }
  //print_conditional_to_stream(out_stream, asr_config.sigma_asr_generator_obs_conditionals[0][0]);
  ///print_conditional_to_stream(out_stream, asr_config.sigma_asr_generator_obs_conditionals[1][0]);
  //print_conditional_to_stream(out_stream, asr_config.sigma_asr_generator_obs_conditionals[0][1]);
  //print_conditional_to_stream(out_stream, asr_config.sigma_asr_generator_obs_conditionals[1][1]);
}
void sigma_asr_generator_print_preamble
(
  FILE * out_stream
)
{

}

void sigma_asr_generator_print_postamble
(
 FILE * out_stream
)
{

}
