//
//  sigma_conditional.c
//  ASR Utils
//
//  Created by Himanshu Joshi on 1/25/16.
//  Copyright © 2016 Take Infinity. All rights reserved.
//

#include "sigma_conditional.h"

void print_predicate_declaration_to_stream
(
  FILE                   * out_stream,
  sigma_predicate_s      * predicate_declaration
)
{
  int num_elem;
  char* world = NULL;
  char* select = NULL;
  sigma_pred_dim_type_var_name_s* this_dim = NULL;

  do{

    if(out_stream == NULL || predicate_declaration == NULL) break;

    if(predicate_declaration->num_dims == 0)
    {
      printf("predicate: %s has 0 dims, not allowed \n",predicate_declaration->predicate_name);
    }

    fprintf(out_stream,"  (predicate '%s\n",predicate_declaration->predicate_name);

    world = ((predicate_declaration->world == SIGMA_PRED_CLOSED_WORLD) ? "closed" :"open" );
      fprintf(out_stream,"             :world '%s\n",world);

    if(predicate_declaration->is_perceptual)
      fprintf(out_stream,"             :perception t\n");
      
    if(predicate_declaration->predicate_specific_learning_rate)
      fprintf(out_stream,"             :learning-rate %f\n",predicate_declaration->learning_rate);
      
    fprintf(out_stream,"             :arguments '(");
    for(num_elem =0  ; num_elem < predicate_declaration->num_dims; num_elem++)
    {
      this_dim = &predicate_declaration->predicate_dim[num_elem];
      select = (this_dim->selection_type == SIGMA_PRED_CLOSED_BEST_SEL ) ? " !" : ( (this_dim->selection_type == SIGMA_PRED_CLOSED_MAINTAIN_ALL)  ? " %": "");
      fprintf(out_stream,"(%s %s%s) ",this_dim->var_name, this_dim->type_name,select);
    }
    fprintf(out_stream,")\n");
    if(predicate_declaration->function_name[0] != '\x0')
      fprintf(out_stream,"             :function %s\n",predicate_declaration->function_name);

    fprintf(out_stream,"  )\n");
      
  }while(0);

  return;
}/* print_predicate_declaration_to_stream */
void print_predicate_pattern_to_stream
(
  FILE                           * out_stream,
  sigma_predicate_pattern_s      * predicate_pattern
)
{

  int num_elem;
  sigma_pred_pattern_dim_type_s* this_dim = NULL;

  do{
    if(out_stream == NULL || predicate_pattern == NULL) break;
    if(predicate_pattern->num_dims < 1 ) break;

    fprintf(out_stream,"(%s ",predicate_pattern->parent_pred->predicate_name);
    for(num_elem =0; num_elem < predicate_pattern->num_dims; num_elem++)
    {
      this_dim = &predicate_pattern->predicate_dim[num_elem];

      fprintf(out_stream,"(%s ",this_dim->type_name);

      if(this_dim->val_var_name[0] == (char)'\x0')
        printf("Unexpected : empty var name for predicate %s\n",
               predicate_pattern->parent_pred->predicate_name);

      if(this_dim->is_val == true)
      {
        fprintf(out_stream,"%s",this_dim->val_var_name);
      }
      else
      {
        fprintf(out_stream,"(%s)",this_dim->val_var_name);
      }
      fprintf(out_stream,") ");
    }
    fprintf(out_stream,")\n");

  }while(0);

} /*  print_condition_to_stream */

void convert_predicate_decl_2_predicate_pattern
(
 sigma_predicate_s         * from_pred,
 sigma_predicate_pattern_s * to_pattern
)
{
  int dim = 0;
  to_pattern->parent_pred = from_pred;
  to_pattern->num_dims = from_pred->num_dims;

  for(dim = 0; dim < from_pred->num_dims && dim < MAX_PRED_DIM; dim++)
  {
    to_pattern->predicate_dim[dim].type_name = from_pred->predicate_dim[dim].var_name;
    //Do not fill in the variable name, as that will be filled later
  }

}
void print_conditional_to_stream
(
  FILE                * out_stream, 
  sigma_conditional_s * conditional_to_print
)
{
  int num_elem = 0;
  /*  print the conditional to the stream provided */
  do{
    if(out_stream == NULL || conditional_to_print == NULL)
    {
      printf("No out stream provided \n");
      break;
    }

    fprintf(out_stream,"  (CONDITIONAL '%s\n", conditional_to_print->conditional_name);

    if (conditional_to_print->num_conditions > 0)
      fprintf(out_stream,"               :CONDITIONS '( \n");
    for(num_elem = 0; num_elem < conditional_to_print->num_conditions; num_elem++)
    {
      fprintf(out_stream,"                              ");
      print_predicate_pattern_to_stream(out_stream,conditional_to_print->condition_patterns[num_elem]);
    }
    if (conditional_to_print->num_conditions > 0)
      fprintf(out_stream,"                            )\n");

    if (conditional_to_print->num_actions > 0)
      fprintf(out_stream,"               :ACTIONS   '( \n");
    for(num_elem = 0; num_elem < conditional_to_print->num_actions; num_elem++)
    {
      fprintf(out_stream,"                              ");
      print_predicate_pattern_to_stream(out_stream,conditional_to_print->actions_patterns[num_elem]);
    }
    if (conditional_to_print->num_actions > 0)
      fprintf(out_stream,"                           )\n");

    if (conditional_to_print->num_condacts > 0)
      fprintf(out_stream,"               :CONDACTS  '( \n");
    for(num_elem = 0; num_elem < conditional_to_print->num_condacts; num_elem++)
    {
      fprintf(out_stream,"                              ");
      print_predicate_pattern_to_stream(out_stream,conditional_to_print->condacts_patterns[num_elem]);
    }
    if (conditional_to_print->num_condacts > 0)
      fprintf(out_stream,"                          )\n");

    fprintf(out_stream,"  )\n");
    
  }while(0);
}/*  print_conditional_to_stream */


