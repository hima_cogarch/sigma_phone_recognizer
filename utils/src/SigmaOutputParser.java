import java.io.*;
import java.util.StringTokenizer;

/**
 * Created by hima on 3/21/16.
 */
public class SigmaOutputParser {
    public static int none = 0;
    public static int debug_output = 0x1;
    public static int flag_errors = 0x2;


    public static int debug_level = 0x2;
    public static void parse_sigma_output(BufferedReader input, Writer output) throws IOException {
        int num_sent_processed = 0;

        do{
            if(input == null || output == null) break;

            output.write("#!MLF!#");
            String prev_phone = null;
            String this_sent = null;
            do{
                String this_line = input.readLine();
                if(this_line == null) break;

                StringTokenizer line_tokens = new StringTokenizer(this_line,"[");
                int num_toks = line_tokens.countTokens();
                if(num_toks == 1) {
                    if(num_sent_processed > 0 ) output.write(".\n");
                    output.write(this_line + "\n");
                    num_sent_processed++;
                    prev_phone = null;
                    this_sent = this_line;
                }
                else{

                    if((debug_level & debug_output) > 0)
                        System.out.println("Num tokens "+line_tokens.countTokens());
                    String this_tok = null;
                    for (int tok = 0; tok < num_toks; tok++)
                    {
                        this_tok = line_tokens.nextToken();
                        //System.out.println(line_tokens.nextToken("])") + " <--" );
                    }
                    StringTokenizer tok_tokens = new StringTokenizer(this_tok,"]");
                    String this_phone = tok_tokens.nextToken();
                    if((debug_level & debug_output) > 0)
                        System.out.println(this_phone + " <--- ");
                    output.write(this_phone.toLowerCase()+"\n");

                    if((debug_level & flag_errors) > 0) {
                        if (prev_phone != null) {
                            if (this_phone.equals(prev_phone)) {
                                output.write("Error : " + this_sent + " phone " + this_phone + " extracted twice \n");
                            }
                        }
                    }
                    prev_phone = this_phone;
                }
            }while(true);
        }while(false);
        return;
    }
    public static void main(String[] args) throws IOException, FileNotFoundException {

        OutputStream program_out = null;
        FileReader input_file   = null;

        if(args.length < 1 ){
            System.out.println("Input file missing");

            return;
        }
        else{
            input_file = new FileReader(args[0]);
        }

        if(args.length < 2)
        {
            program_out = System.out;
        }
        else {
            program_out = new FileOutputStream(args[1]);
        }

        Writer out_writer = new OutputStreamWriter(program_out);
        BufferedReader fileReader = new BufferedReader(input_file);


        //now parse and generate output
        parse_sigma_output(fileReader,out_writer);
        out_writer.close();
        fileReader.close();
    }
}
