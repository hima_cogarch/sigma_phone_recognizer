import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Created by hima on 2/24/16.
 */
public class htk_slf_parser {

    int num_nodes;
    int num_links;


    ArrayList<String> id_to_phone;
    HashMap<String, Integer> phone_id_map;
    //We assume an entry state, an exit state and a dense transition matrix
    // so the total links is : (num_nodes - 2) * (num_nodes -1) + (num_nodes) = (num_nodes * (num_nodes - 1))
    double lattice_network[][];
    //for now we pass in the expected number of links in the constructor
    public htk_slf_parser(String slf_path) throws IOException {
        //any variables
        String line ;
        String top_token;
        String next_token;
        boolean unexp_token = false;
        StringTokenizer space_tokenizer;
        StringTokenizer keyword_tokenizer;
        String temp ;

        FileReader fileReader = new FileReader(slf_path);

        // Always wrap FileReader in BufferedReader.
        BufferedReader bufferedReader =
                new BufferedReader(fileReader);


        do{
            if(slf_path == null)
            {
                System.out.println("no input path specified, bailing out ");
                break;
            }

            line = bufferedReader.readLine();
            space_tokenizer = new StringTokenizer(line);
            top_token = (String)space_tokenizer.nextElement();
            boolean num_nodes_found = false;

            if(top_token.equals("VERSION=1.0") == false){
                System.out.println("Expected version string");
            }
            //slight bug here; TODO: FIX IT! what if there is no VERSION STring?

            line = bufferedReader.readLine();
            space_tokenizer = new StringTokenizer(line);
            //first get the number of nodes
            keyword_tokenizer = new StringTokenizer((String)space_tokenizer.nextElement(),"=");
            temp = keyword_tokenizer.nextToken(); temp = keyword_tokenizer.nextToken();
            this.num_nodes = Integer.parseInt(temp);

            keyword_tokenizer = new StringTokenizer((String)space_tokenizer.nextElement(),"=");
            temp = keyword_tokenizer.nextToken(); temp = keyword_tokenizer.nextToken();
            this.num_links = Integer.parseInt(temp);

            System.out.println("Reading HTK SLF with "+this.num_nodes +" nodes and "+ this.num_links + " links.");
/*
            if((this.num_nodes-1) * num_nodes != this.num_links) {
                System.out.println("Unexpected number of links, bailing out");
                break;
            }
*/
            phone_id_map = new HashMap<String, Integer>();
            id_to_phone  = new ArrayList<String>();

            lattice_network = new double[num_nodes][num_nodes];
            //reset everything to 0.0
            for(int from =0; from < num_nodes;from++)
            {
                for(int to =0; to < num_nodes; to++)
                {
                    lattice_network[from][to] = 0.0;
                }
            }

            //get the nodes
            int from = 0;
            int to   = 0;
            int phone_id = 0;

            do{
                line = bufferedReader.readLine();

                space_tokenizer = new StringTokenizer(line);
                keyword_tokenizer = new StringTokenizer((String)space_tokenizer.nextToken(), "=");
                top_token = keyword_tokenizer.nextToken();
                if(top_token.equals("I") == false) {
                    System.out.println("Unexpected token received bailing out at phone id... "+phone_id);
                    break;
                }
                int this_id = Integer.parseInt(keyword_tokenizer.nextToken());
                if(this_id != phone_id) {
                    System.out.println("Unexpected phone id " + this_id +  " expected phone id... "+phone_id);
                    break;
                }

                keyword_tokenizer = new StringTokenizer((String)space_tokenizer.nextToken(), "=");
                top_token = keyword_tokenizer.nextToken();
                if(top_token.equals("W") == false) {
                    System.out.println("Unexpected token received bailing out at phone id... "+phone_id);
                    break;
                }
                String this_phone_name = keyword_tokenizer.nextToken();
                //TODO: check to see if this phone is already in the hash
                phone_id_map.put(this_phone_name,this_id);
                id_to_phone.add(phone_id,this_phone_name);
                phone_id++;
            }while(phone_id < num_nodes);

            //Now all the links;
            int link_id=0;
            do{
                line = bufferedReader.readLine();

                space_tokenizer = new StringTokenizer(line);
                keyword_tokenizer = new StringTokenizer((String)space_tokenizer.nextToken(), "=");
                top_token = keyword_tokenizer.nextToken();
                if(top_token.equals("J") == false) {
                    System.out.println("Unexpected token received bailing out at phone id... "+link_id);
                    break;
                }
                int this_id = Integer.parseInt(keyword_tokenizer.nextToken());
                if(this_id != link_id) {
                    System.out.println("Unexpected phone id " + this_id +  " expected phone id... "+link_id);
                    break;
                }

                keyword_tokenizer = new StringTokenizer((String)space_tokenizer.nextToken(), "=");
                top_token = keyword_tokenizer.nextToken();
                if(top_token.equals("S") == false) {
                    System.out.println("Unexpected token received bailing out at phone id... "+link_id);
                    break;
                }
                int start_phone = Integer.parseInt(keyword_tokenizer.nextToken());
                keyword_tokenizer = new StringTokenizer((String)space_tokenizer.nextToken(), "=");
                top_token = keyword_tokenizer.nextToken();
                if(top_token.equals("E") == false) {
                    System.out.println("Unexpected token received bailing out at phone id... "+link_id);
                    break;
                }
                int end_phone = Integer.parseInt(keyword_tokenizer.nextToken());

                keyword_tokenizer = new StringTokenizer((String)space_tokenizer.nextToken(), "=");
                top_token = keyword_tokenizer.nextToken();
                if(top_token.equals("l") == false) {
                    System.out.println("Unexpected token received bailing out at phone id... "+link_id);
                    break;
                }
                double likelihood = Double.parseDouble(keyword_tokenizer.nextToken());
                //TODO: check to see if this phone is already in the hash
                //.put(this_phone_name,this_id);
                lattice_network[start_phone][end_phone] = Math.exp(likelihood);

                link_id++;

            }while(link_id<num_links);


            for(from =0; from < num_nodes; from++) {
                for (to = 0; to < num_nodes; to++) {
                    if(lattice_network[from][to] < 0.00 || lattice_network[from][to] > 1.00) {
                        System.out.println("Unexpected value at ("+from+","+to+")");
                    }
                }
            }


            //

            System.out.println("Parsed slf "+slf_path+" file with "+num_nodes + " nodes and "+ num_links + " links ");
        }while(false);
    }
    public ArrayList<String> get_exit_trans_probs() {
        ArrayList<String> inter_trans_func = new ArrayList<String>();
        do {
            //start from 1 and dont count the exit state transition;
            for (int from = 1; from < num_nodes; from++) {
                    inter_trans_func.add("(" + lattice_network[from][49] + " " + id_to_phone.get(from) + " s2 "
                            + id_to_phone.get(49) + " s0" + ")");
            }
        } while (false);
        return inter_trans_func;
    }
    public ArrayList<String> get_init_trans_probs()
    {
        ArrayList<String> inter_trans_func = new ArrayList<String>();
        do{
            //start from 1 and dont count the exit state transition;
            for(int to =1; to < num_nodes -1; to++){
                //inter_trans_func.add("(" + lattice_network[0][to] + " " + id_to_phone.get(0) +" s2 " + id_to_phone.get(to) + " s0" + ")");
                inter_trans_func.add("(" + lattice_network[0][to] + " " + id_to_phone.get(to) + " s0" + ")");
                }
        }while (false);
        return inter_trans_func;
    }
    public ArrayList<String> get_inter_phone_trans()
    {
        ArrayList<String> inter_trans_func = new ArrayList<String>();
        do{
            //start from 1 and dont count the exit state transition;
            for(int from =1; from < num_nodes - 1; from++) {
                for(int to =1; to < num_nodes -1; to++){
                    //inter_trans_func.add("(" + lattice_network[from][to] + " " + id_to_phone.get(from) +" s2 " + id_to_phone.get(to) + " s0" + ")");
                    inter_trans_func.add("(" + lattice_network[from][to] + " " + id_to_phone.get(from) +" s2 " + ")");
                }
            }
        }while (false);
        return inter_trans_func;

    }
}
