//
//  sigma_utils.c
//  ASR Utils
//
//  Created by Himanshu Joshi on 1/11/16.
//  Copyright © 2016 Take Infinity. All rights reserved.
//

#include "bsp.h"
#include "sigma_utils.h"
#include "common_includes.h"
#include <stdio.h>
#include "timit_defines.h"

#define MAX_NUM_HMM_STATES 3
#define PROB_FLOAT_FIDELTY 8

char* hmm_state_names[] = { "s0", "s1", "s2"};

typedef struct
{

  float s0_to_s0;
  float s0_to_s1;
  float s0_to_s2;
  float s1_to_s1;
  float s1_to_s2;
  float s2_to_s2;

}hmm_trans_template_params_s;

typedef struct {

  int num_states;

  bool allow_plus2_trans;

  hmm_trans_template_params_s trans_params;

}hmm_gen_trans_parameters;

typedef struct {

  bool use_max_entropy_between_phones;
  char def_factor_name[32];
  hmm_gen_trans_parameters intra_hmm_params;

}generate_sigma_trans_factor_db_s;

generate_sigma_trans_factor_db_s generate_sigma_trans_factor_db =
{

  .use_max_entropy_between_phones = false,

  .def_factor_name = "*global-phone-trans-fn*", //TODO this must be merged with te other asr generator file

  .intra_hmm_params = {
    .num_states = MAX_NUM_HMM_STATES,
    .allow_plus2_trans = true,
    .trans_params = {
      .s0_to_s0 = 0.8,
      .s0_to_s1 = 0.1,
      .s0_to_s2 = 0.1,
      .s1_to_s1 = 0.8,
      .s1_to_s2 = 0.2,
      .s2_to_s2 = 0.99,
    },
  },

};


const char* test_list_phones[] = { "sil", "aa"};

extern const char* timit_train_phone_list[];

bool generate_sigma_transition_factor(FILE* out_file)
{
  bool ret_val = false;
  generate_sigma_trans_factor_db_s* trans_db = &generate_sigma_trans_factor_db;
  hmm_gen_trans_parameters * intra_hmm_params = &trans_db->intra_hmm_params;
  int num_train_phones = timit_defines_num_timit_phones;
  //sizeof(timit_train_phone_list)/sizeof(timit_train_phone_list[0]);
  //int num_train_phones = sizeof(test_list_phones)/sizeof(test_list_phones[0]); //timit_defines_num_timit_phones; //sizeof(timit_train_phone_list)/sizeof(timit_train_phone_list[0]);

  do{

    if(out_file == NULL) out_file = stdout;

    //First generate the default

    fprintf(out_file, "(defparameter %s '( \n (0 * * * *)  \n",trans_db->def_factor_name);

    //Now, for each phone, first generate the intra HMM transition,
    //  followed by inter HMM transitions
    for (int phone = 0 ; phone < num_train_phones; phone++) {
      const char* this_phone = timit_train_phone_list[phone];
      //char* this_phone = timit_train_phone_list[phone];

      // (prob_value HAPPENED PREV-PHONE PREV-STATE NEXT-PHONE NEXT-STATE)
      // 0 -> 0, 0 -> 1, 0 -> 2
      fprintf(out_file, "  (%2.8f  %s %s %s %s) ",intra_hmm_params->trans_params.s0_to_s0,
               this_phone, hmm_state_names[0], this_phone, hmm_state_names[0]);
      fprintf(out_file, "  (%2.8f %s %s %s %s) ",intra_hmm_params->trans_params.s0_to_s1,
               this_phone, hmm_state_names[0], this_phone, hmm_state_names[0+1]);
      fprintf(out_file, "  (%2.8f %s %s %s %s) ",intra_hmm_params->trans_params.s0_to_s2,
              this_phone, hmm_state_names[0], this_phone, hmm_state_names[0+2]);

      //1 -> 1 & 1->2
      fprintf(out_file, "  (%2.8f %s %s %s %s) ",intra_hmm_params->trans_params.s1_to_s1,
              this_phone, hmm_state_names[1], this_phone, hmm_state_names[1]);
      fprintf(out_file, "  (%2.8f %s %s %s %s) ",intra_hmm_params->trans_params.s1_to_s2,
               this_phone, hmm_state_names[1], this_phone, hmm_state_names[1+1]);

      fprintf(out_file, "  (%2.8f %s %s %s %s) ",intra_hmm_params->trans_params.s2_to_s2,
               this_phone, hmm_state_names[2], this_phone, hmm_state_names[2]);

      //Now for transitions out of 2 that result in inter-phone transitions
      float residual_prob_value = 1.0 - intra_hmm_params->trans_params.s2_to_s2; //how much spillover from each phone?
      float inter_phone_prob = residual_prob_value/(float)(num_train_phones); //max entropy between phones to begin with, this should come from language models

      for(int nphone = 0; nphone < num_train_phones; nphone++)
      {
        fprintf(out_file, "  (%2.8f %s %s %s %s) ", inter_phone_prob,
                this_phone, "s2",timit_train_phone_list[nphone], "s0" );
      }

      fprintf(out_file, "\n");
    }

    //The end brackets
    fprintf(out_file," ) ) \n");

    fflush(out_file);
    ret_val = true;

  }while(0);

  return ret_val;
}
