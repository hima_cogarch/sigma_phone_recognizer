//
//  bsp.h
//  ASR Utils
//
//  Created by Himanshu Joshi on 1/11/16.
//  Copyright © 2016 Take Infinity. All rights reserved.
//

#ifndef bsp_h
#define bsp_h

//All sigma specific definitions go here:

#define MAX_NAME_LEN 0xFF

#define MAX_PRED_DIM 8

#define MAX_PRED_IN_PATTERN 16

#define MAX_PATTERNS_IN_CONDITIONAL 64

#define MAX_CONDITIONS_IN_CONDITIONAL MAX_PATTERNS_IN_CONDITIONAL

#define MAX_ACTIONS_IN_CONDITIONAL MAX_PATTERNS_IN_CONDITIONAL

#define MAX_CONDACTS_IN_CONDITIONAL MAX_PATTERNS_IN_CONDITIONAL

// All the top level paths are specified here

#define BSP_GMTK_TKSRC "/Users/hima/developer/GMTK/"

#define BSP_GMTK_TIMIT_FILES "/Users/hima/developer/GMTK/GMTK_TIMIT_tutorial-1.2.0-beta"

#define BSP_TIMIT_PHONE_LIST "/Users/hima/developer/sigma_continuous_speech_project/utils/TIMIT_data/phone_list_49.txt"

#endif /* bsp_h */
