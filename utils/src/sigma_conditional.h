//
//  sigma_conditional.h
//  ASR Utils
//
//  Created by Himanshu Joshi on 1/25/16.
//  Copyright © 2016 Take Infinity. All rights reserved.
//

#ifndef sigma_conditional_h
#define sigma_conditional_h

#include <stdio.h>
#include "bsp.h"
#include <stdbool.h>

typedef enum{
  SIGMA_PRED_ENUM,
  SIGMA_PRED_INT,
  SIGMA_PRED_CONT,
  SIGMA_PRED_BOOL

}sigma_predicate_dim_type_e;

typedef enum {
  SIGMA_PRED_UNIQUE,
  SIGMA_PRED_UNIVERSAL
}sigma_predicate_selection_type_e;

typedef enum {
  SIGMA_PRED_OPEN_WORLD,
  SIGMA_PRED_CLOSED_WORLD,
  SIGMA_PRED_DEF_WORLD
}sigma_predicate_world_type_e;

typedef enum {
  SIGMA_PRED_NO_REPLACE,
  SIGMA_PRED_REPLACE
}sigma_predicate_replace_e;

typedef enum {
  SIGMA_PRED_CLOSED_NONE,            //not sure this is allowed/needed
  SIGMA_PRED_CLOSED_BEST_SEL,
  SIGMA_PRED_CLOSED_RANDOM_SEL,
  SIGMA_PRED_CLOSED_MAINTAIN_ALL,
  SIGMA_PRED_CLOSED_BOLTZMANN
}sigma_predicate_dim_selection_type_e;

typedef struct
{
  char                                  type_name[MAX_NAME_LEN];
  char                                  var_name[MAX_NAME_LEN];
  sigma_predicate_dim_selection_type_e  selection_type;

}sigma_pred_dim_type_var_name_s;

typedef struct
{
  //This is the variable name in the predicate definition.
  char                                 * type_name;
  //TRUE - If the predicate pattern declares a value such as (phone (phone))
  //FALSE - If the predicate pattern declares a variable to be used (phone sil)
  bool                                  is_val;
  //The value or variable name used here.
  char                                  val_var_name[MAX_NAME_LEN];

}sigma_pred_pattern_dim_type_s;


typedef struct{

  //A predicate must have at least 1 dimension
  char                           predicate_name[MAX_NAME_LEN];

  int                            num_dims;
  //This should contain links to dimensions in the predicate
  sigma_pred_dim_type_var_name_s predicate_dim[MAX_PRED_DIM];

  //World
  sigma_predicate_world_type_e   world;

  //Replace?
  sigma_predicate_replace_e      replace;

  //perception ?
  bool                           is_perceptual;

  //special learning rate, use 0.0 to turn it off
  bool                           predicate_specific_learning_rate;
  double                         learning_rate;

  //function name, use an empty name to have this turned off
  bool                           has_function;
  char                           function_name[MAX_NAME_LEN];
}sigma_predicate_s;

/*
 A predicate pattern used in a conditional is slighlty different than the predicate
 definition 
*/
typedef struct
{
  //A predicate must have at least 1 dimension
  int                            num_dims;
  //This should contain links to dimensions in the predicate
  sigma_pred_pattern_dim_type_s  predicate_dim[MAX_PRED_DIM];

  //the predicate defintion from the parent pred
  sigma_predicate_s             * parent_pred;
}sigma_predicate_pattern_s;

typedef struct{

  char conditional_name[MAX_NAME_LEN];

  int num_conditions;
  sigma_predicate_pattern_s  * condition_patterns[MAX_CONDITIONS_IN_CONDITIONAL];

  int num_actions;
  sigma_predicate_pattern_s  * actions_patterns[MAX_ACTIONS_IN_CONDITIONAL];

  int num_condacts;
  sigma_predicate_pattern_s  * condacts_patterns[MAX_CONDACTS_IN_CONDITIONAL];

}sigma_conditional_s;


/* Function declarations */
void print_predicate_declaration_to_stream
(
  FILE                   * out_stream,
  sigma_predicate_s      * predicate_declaration
);

void print_predicate_pattern_to_stream
(
  FILE                           * out_stream,
  sigma_predicate_pattern_s      * predicate_declaration
);

void print_conditional_to_stream
(
  FILE                * out_stream,
  sigma_conditional_s * conditional_to_print
);

//helper function to convert a predicate definiton to a predicate pattern
void convert_predicate_decl_2_predicate_pattern
(
  sigma_predicate_s         * from_pred,
  sigma_predicate_pattern_s * to_pattern
);
#endif /* sigma_conditional_h */
