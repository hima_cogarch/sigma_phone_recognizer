//
//  main.c
//  ASR Utils
//
//  Created by Himanshu Joshi on 1/11/16.
//  Copyright © 2016 Take Infinity. All rights reserved.
//

//launch various utilities
#include <stdio.h>
#include "common_includes.h"
#include "sigma_utils.h"
#include "sigma_asr_generator.h"

#define PRINT_ALL_PREDS 0x01
#define PRINT_OBS_PREDS 0x02
#define PRINT_ALL_CONDS 0x04
#define PRINT_TRANS_FN  0x08
#define PRINT_OBS_CONDS 0x10
#define PRINT_TRANS_CONDS 0x20
#define PRINT_PREAMBLE  0x40
#define PRINT_POSTAMBLE 0x80

typedef struct
{

  uint32_t print_config;
}print_config_s;

print_config_s print_config =
{
  .print_config = (PRINT_OBS_CONDS | PRINT_TRANS_CONDS),
};


int main(int argc, const char * argv[]) {

  bool work_tbd = 0;
  FILE* outfile        = NULL;
  uint32_t * print_cfg = &print_config.print_config;
  //  bool ret_val;

  //HACK HACK HACK for now, open this file somewhere else
  //phone_list_file = fopen((const char *)BSP_TIMIT_PHONE_LIST, "r");

  //HACK do this somewhere else
  if(argc > 1){
    outfile = fopen(argv[1], "w+");
  }

  //if(!phone_list_file)
  //{
  //  printf("bailing out, could not open input file %s \n",BSP_GMTK_TIMIT_FILES);
  //}
  //Check to see any BSP options should be overridden here..

  //Check to see what is asked to be done here...

  work_tbd = false;
  //Now do it
  if( *print_cfg & PRINT_TRANS_FN)
  {
    //For now we have only 1 thing to be done
    generate_sigma_transition_factor(stdout);
  }

  sigma_asr_generator_init();
  //Now make the predicates and conditionals
  //TODO: Pass any specific parameters here
  sigma_asr_generator_make_predicates();
  sigma_asr_generator_make_conditionals();

  //Now print the ASR... starting with the preamble

  sigma_asr_generator_print_preamble(stdout);
  if( *print_cfg & PRINT_ALL_PREDS)
  {
    //For now we have only 1 thing to be done
    sigma_asr_generator_print_asr_predicates(stdout);
  }
  if( *print_cfg & (PRINT_OBS_CONDS | PRINT_TRANS_CONDS))
  {
    //For now we have only 1 thing to be done
    sigma_asr_generator_print_asr_conditionals(stdout);
  }
  if( *print_cfg & PRINT_POSTAMBLE)
  {
    //For now we have only 1 thing to be done
    sigma_asr_generator_print_postamble(stdout);
  }


  //Convert boolean into a 0 or non-zero value for the OS
  return 0;
}
