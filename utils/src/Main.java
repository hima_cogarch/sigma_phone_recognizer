/*
 * Read HTK HMM files
 */


import java.io.*;
import java.util.ArrayList;

public class Main {

    public static String hmm_base_path = "/Users/hima/developer/phone_classifier/htk_data/";
    public static String hmm_path = hmm_base_path + "hmms/";
    public static String hmm_list_path = hmm_base_path + "train_monophones";
    public static String hmm_slf = hmm_base_path + "outLatFile_nonwsj_lm";

    private static String obs_fn_dim0 = new String("*global-phone-obs-dim0-fn*");
    private static String obs_fn_dim1 = new String("*global-phone-obs-dim1-fn*");
    private static String obs_fn_dim2 = new String("*global-phone-obs-dim2-fn*");
    private static String obs_fn_dim3 = new String("*global-phone-obs-dim3-fn*");
    private static String tran_fn     = new String("**global-phone-trans-fn**");

    private static ArrayList<ArrayList<String>> obs_fun_dim0;
    private static ArrayList<ArrayList<String>> obs_fun_dim1;
    private static ArrayList<ArrayList<String>> obs_fun_dim2;
    private static ArrayList<ArrayList<String>> obs_fun_dim3;
    private static ArrayList<ArrayList<String>> trans_func;

    private static String default_output_string = new String ("; File contains all the factors generated from HTK \n");
    static private ArrayList<String> hmms;
    static private ArrayList<hmm_parser> hmm_defs;

    public static void main(String[] args) throws IOException {
        System.out.println(hmm_base_path);
        System.out.println(hmm_path);
        System.out.println(hmm_list_path);
        String line = null;
        hmms = new ArrayList<String>();
        hmm_defs = new ArrayList<hmm_parser>();
        OutputStream write_output = System.out;


        //expect the file name to print all the lists
        if(args.length > 0){
            String out_stream_name = hmm_base_path + args[0];
            //Now try to open it for writing
            try {
                write_output = new FileOutputStream(out_stream_name);
            }
            catch (FileNotFoundException fe){
                write_output = System.out;
                System.out.println("Cannot open file "+ args[1] + " for writing, output directed to stdout ");
            }
        }
        Writer       out_writer = new OutputStreamWriter(write_output);
        /*
            String testDouble = "6.525000e-01";
            double parsedDouble = Double.parseDouble(testDouble);
            System.out.println("Parsed the double "+testDouble + " as "+ parsedDouble);
        */
        FileReader fileReader = new FileReader(hmm_list_path);

        // Always wrap FileReader in BufferedReader.
        BufferedReader bufferedReader =
                new BufferedReader(fileReader);

        do{
            line = bufferedReader.readLine();
            if(line == null) break;
            hmms.add(line);
        }while(true);

        System.out.println("Reading "+ hmms.size() + " the HMMs from... " + hmm_list_path );
        System.out.println("stored at... " + hmm_path );


        //Now, read in the HMMs, one by one
        for(int i =0; i< hmms.size(); i++)
        {
            String this_name = hmms.get(i);
            hmm_parser this_hmm = new hmm_parser();

            boolean parse_success  = this_hmm.hmm_parser_parse_file(hmm_path + hmms.get(i), hmms.get(i));
            if(parse_success == true) hmm_defs.add(this_hmm);
            else System.out.println("---> Error parsing hmm "+this_name);
        }

        //Now print them all
        System.out.println("Read "+hmm_defs.size()+" hmms from "+hmm_base_path);
        //hmm_defs.printhmm
        obs_fun_dim0 = new ArrayList<ArrayList<String>>();
        obs_fun_dim1 = new ArrayList<ArrayList<String>>();
        obs_fun_dim2 = new ArrayList<ArrayList<String>>();
        obs_fun_dim3 = new ArrayList<ArrayList<String>>();
        trans_func   = new ArrayList<ArrayList<String>>();

        for(int i =0; i< hmms.size(); i++){
            hmm_parser this_hmm = hmm_defs.get(i);
            obs_fun_dim0.add(this_hmm.get_obs_func(0));
        }
        for(int i =0; i< hmms.size(); i++){
            hmm_parser this_hmm = hmm_defs.get(i);
            obs_fun_dim1.add(this_hmm.get_obs_func(1));
        }
        for(int i =0; i< hmms.size(); i++){
            hmm_parser this_hmm = hmm_defs.get(i);
            obs_fun_dim2.add(this_hmm.get_obs_func(2));
        }
        for(int i =0; i< hmms.size(); i++){
            hmm_parser this_hmm = hmm_defs.get(i);
            obs_fun_dim3.add(this_hmm.get_obs_func(3));
        }

        //Now, read in the HMMs, one by one
        for(int i =0; i< hmms.size(); i++)
        {
            hmm_parser this_hmm = hmm_defs.get(i);
            trans_func.add(this_hmm.get_trans_fun());
        }


        //Now read the lattice.
        htk_slf_parser phone_network_parse = new htk_slf_parser(hmm_slf);

        //Now dump the sigma predicates etc.
        trans_func.add(phone_network_parse.get_inter_phone_trans());

        ArrayList<String> priors     = phone_network_parse.get_init_trans_probs();
        ArrayList<String> exit_probs = phone_network_parse.get_exit_trans_probs();

        //Now that everything has been assembled, for each dimension of the observation function
        //  and the transition functions, print the outputs.
        //Start with observation function

        out_writer.write(default_output_string);
        out_writer.write("\n\n");
        out_writer.write("; Observation function dim0\n");
        out_writer.write("(defparameter "+ obs_fn_dim0 + " '( \n");
        for(int hmm = 0; hmm< obs_fun_dim0.size();hmm++) {
            ArrayList<String> this_fun = obs_fun_dim0.get(hmm);
            for (int obs = 0; obs < this_fun.size(); obs++) {
                out_writer.write(this_fun.get(obs));
                out_writer.write(" \n");
            }
        }
        out_writer.write(" ))\n");
        out_writer.write("; Observation function dim1\n");
        out_writer.write("(defparameter "+ obs_fn_dim1 + " '( \n");
        for(int hmm = 0; hmm< obs_fun_dim0.size();hmm++) {
            ArrayList<String> this_fun = obs_fun_dim1.get(hmm);
            for (int obs = 0; obs < this_fun.size(); obs++) {
                out_writer.write(this_fun.get(obs));
                out_writer.write(" \n");
            }
        }
        out_writer.write(" ))\n");
        out_writer.write("; Observation function dim2\n");
        out_writer.write("(defparameter "+ obs_fn_dim2 + "  '(\n");
        for(int hmm = 0; hmm< obs_fun_dim0.size();hmm++) {
            ArrayList<String> this_fun = obs_fun_dim2.get(hmm);
            for (int obs = 0; obs < this_fun.size(); obs++) {
                out_writer.write(this_fun.get(obs));
                out_writer.write(" \n");
            }
        }
        out_writer.write(" ))\n");
        out_writer.write("; Observation function dim3\n");
        out_writer.write("(defparameter "+ obs_fn_dim3 + "  '(\n");
        for(int hmm = 0; hmm< obs_fun_dim0.size();hmm++) {
            ArrayList<String> this_fun = obs_fun_dim3.get(hmm);
            for (int obs = 0; obs < this_fun.size(); obs++) {
                out_writer.write(this_fun.get(obs));
                out_writer.write(" \n");
            }
        }
        out_writer.write(" ))\n");

        //Now do the transition function
        out_writer.write("; Global transition function\n");
        out_writer.write("(defparameter "+ tran_fn + "  '(\n");
        for(int hmm = 0; hmm< trans_func.size();hmm++) {
            ArrayList<String> this_trans_list = trans_func.get(hmm);
            for (int trans_id = 0; trans_id < this_trans_list.size(); trans_id++) {
                out_writer.write(this_trans_list.get(trans_id));
                out_writer.write(" \n");
            }
        }
        out_writer.write(" ))\n");

        out_writer.write("; Global sentence priors function\n");
        out_writer.write("(defparameter  *global_utterance_prior*  '(\n");
        for(int trans_id = 0;  trans_id < priors.size();trans_id++){
            out_writer.write(priors.get(trans_id));
            out_writer.write(" \n");
        }
        out_writer.write(" ))\n");

        out_writer.write("; Global sentence exit \n");
        out_writer.write("(defparameter  *global_utterance_end*  '(\n");
        for(int trans_id = 0;  trans_id < exit_probs.size();trans_id++){
            out_writer.write(exit_probs.get(trans_id));
            out_writer.write(" \n");
        }
        out_writer.write(" ))\n");


        out_writer.flush();
        out_writer.close();
        write_output.close();
    }
}
