//
//  common_includes.h
//  ASR Utils
//
//  Created by Himanshu Joshi on 1/11/16.
//  Copyright © 2016 Take Infinity. All rights reserved.
//

#ifndef common_includes_h
#define common_includes_h

#include <stdbool.h>
#include "bsp.h"
#include "sigma_conditional.h" //this has definitions of predicate and conditional types

#endif /* common_includes_h */
