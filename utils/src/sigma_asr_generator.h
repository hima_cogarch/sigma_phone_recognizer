//
//  sigma_asr_generator.h
//  ASR Utils
//
//  Created by Himanshu Joshi on 1/25/16.
//  Copyright © 2016 Take Infinity. All rights reserved.
//

#ifndef sigma_asr_generator_h
#define sigma_asr_generator_h

#include <stdio.h>
#include "bsp.h"


void sigma_asr_generator_make_predicates
(
 void
 );
void sigma_asr_generator_make_conditionals
(
 void
 );

void sigma_asr_generator_print_preamble
(
 FILE * out_stream
 );

void sigma_asr_generator_print_postamble
(
 FILE * out_stream
 );

void sigma_asr_generator_print_asr_conditionals
(
 FILE * out_stream
 );

void sigma_asr_generator_print_asr_predicates
(
  FILE * out_stream
);

void sigma_asr_generator_init
(
  void
);

#endif /* sigma_asr_generator_h */
