import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by hima on 2/17/16.
 */
public class hmm_parser {
    private String hmm_name;
    private int    num_states;

    private int num_streams;

    private int obs_fun[][][];

    private double trans_fun[][];

    public hmm_parser() {

    }
    public hmm_parser(String hmm_name){
        this.hmm_name = hmm_name;
    }
    static private String[] state_names = new String[]{ "s0", "s1", "s2"};

    public ArrayList<String> get_trans_fun(){
        ArrayList<String> ret_val = new ArrayList<String>();
        do{
            for(int from_state = 1 ; from_state < this.num_states -1; from_state++){
                for(int to_state = 1; to_state < this.num_states - 1; to_state++) {
                    ret_val.add("("+this.trans_fun[from_state][to_state]+" "+this.hmm_name
                                +" "+ state_names[from_state-1] + " " + this.hmm_name +
                                " " + state_names[to_state-1]+ ")");
                }
            }

        }while(false);

        return  ret_val;
    }

    public ArrayList<String> get_obs_func(int dimension){
        ArrayList<String> ret_val =null;

        do{
            //only 2 to N-1 states are emission states in HTK HMMs
            if(dimension <0 || dimension >3)
            {
                System.out.println("Bad dimension requested... "+ hmm_name + " doesnt have " +dimension );
                break;
            }
            ret_val = new ArrayList<String>();

            //new int[this.num_states][this.num_streams][256];
            for(int state = 1; state < num_states -1; state++){
                for(int obs = 0; obs < 256; obs++){
                //we return strings of type: (val phone state symbol)
                ret_val.add("("+ Math.exp(this.obs_fun[state][dimension][obs]/2371.8 * -1.0) + " " + this.hmm_name + " " + state_names[state-1] + " "
                               + obs + ")");
                }
            }
        }while(false);
        return ret_val;

    }
    public boolean hmm_parser_parse_file(String path, String name) throws IOException {


        boolean ret_val  = false;
        if(this.hmm_name == null) this.hmm_name = name;
        else {
            //scream error.
        }
        String line ;
        boolean unexp_token = false;

        StringTokenizer space_tokenizer;
        StringTokenizer keyword_tokenizer;

        FileReader fileReader = new FileReader(path);

        // Always wrap FileReader in BufferedReader.
        BufferedReader bufferedReader =
                new BufferedReader(fileReader);


        //Read line by this. This is really hacky and stupid but needs to happen in 1 day.
        // TODO: Fix this by reading binary hmm files but htk doesnt have good documentation.
        do {
            if(name == null) break;
            this.hmm_name = name;
            line = bufferedReader.readLine();
            if (line == null || line.equals("~o") == false) {
                System.out.println("Bad syntax reading " + name);
                break;
            }
            //Check for HMM name:
            while (true) {
                line = bufferedReader.readLine();

                if (line == null) break;
                space_tokenizer = new StringTokenizer(line);
                String this_token = (String)space_tokenizer.nextElement();

                if(this_token.equals("<STREAMINFO>") == true) {
                    this.num_streams = Integer.parseInt((String)space_tokenizer.nextElement());
                    continue;
                }
                if(this_token.equals("<VECSIZE>") == true)    continue;

                //next expected token is the name of the HMM
                if(this_token.equals("~h") == false) {
                    unexp_token = true;
                    break;
                }
                this_token = (String) (space_tokenizer.nextElement());
                if(this_token == null) break;
                if(this_token.equals("\""+hmm_name+"\"") == false){
                    System.out.println("HMM names dont match this name "+ hmm_name + " name from file "+ this_token);
                    unexp_token = true;
                    break;
                }

                //We reached there successfully then we break;
                break;
            }
            if (line == null || unexp_token == true) break;

            System.out.print("Now reading HMM <"+this.hmm_name+"> :");
            //Now that the name has been verified, we read the number of states, the number of observationas has been
            //  verified already

            line = bufferedReader.readLine();
            //This should be the begin HMM line
            if(line.equals("<BEGINHMM>") == false) break;

            line = bufferedReader.readLine();
            if(line == null) break;
            space_tokenizer = new StringTokenizer(line);
            String first_tok = (String)space_tokenizer.nextElement();
            if(first_tok.equals("<NUMSTATES>") == false) break;
            this.num_states = Integer.parseInt((String)space_tokenizer.nextElement());

            if(this.num_states == 0) {
                System.out.println("\n ---> cannot create HMM with 0 states ");
                break;
            }
            this.trans_fun = new double[this.num_states] [this.num_states];
            System.out.print(" with "+this.num_states+" states.\n");

            //hack, fix this, we know all hmms have 4 streams in each state and each stream has 256 mixes
            this.obs_fun = new int[this.num_states][this.num_streams][256];

            //Now parse each state
            for(int s = 1; s < this.num_states -1 && unexp_token == false ; s++) //the very firxt and last states are dummy states in HTK
            {
                unexp_token = false;
                for(int stream = 0; stream < this.num_streams && unexp_token == false; stream ++)
                {
                    int symbol_id = 0;
                    //skip all crap until you get the stream info
                    while(true) {
                        line = bufferedReader.readLine();
                        if (line == null) {
                            unexp_token = true;
                            break;
                        }

                        space_tokenizer = new StringTokenizer(line);
                        String next_token = space_tokenizer.nextToken();
                        if(next_token.equals("<STREAM>")) break;

                    }
                    if(unexp_token == true) break;
                    //Now check for stream info number;
                    int this_stream = Integer.parseInt((String)space_tokenizer.nextToken());
                    if(stream + 1 != this_stream) {
                        unexp_token =true;
                        break;
                    }
                    //Now parse each stream
                    line = bufferedReader.readLine();
                    if(line.equals("<DPROB>") == false) {
                        unexp_token = true;
                        break;
                    }
                    while(true){
                        line = bufferedReader.readLine();
                        space_tokenizer = new StringTokenizer(line);
                        while(space_tokenizer.hasMoreElements())
                        {
                            String this_number = (String)space_tokenizer.nextElement();
                            //NOw check to see if this has more than 1 number
                            StringTokenizer sym_token = new StringTokenizer(this_number,"*");
                            if(sym_token.countTokens() > 1) {
                                //More than 1
                                int symbol = Integer.parseInt((String)sym_token.nextElement());
                                int repeat = Integer.parseInt((String)sym_token.nextElement());
                                for(int r = 0; r < repeat; r++) this.obs_fun[s][stream][symbol_id++] = symbol;
                            }
                            else this.obs_fun[s][stream][symbol_id++] = Integer.parseInt(this_number);

                        }//we just finished reading 1 line;
                        if(symbol_id == 256) break;
                        //More break conditions should be found, else we will never come out of here.

                    }
                    if(symbol_id != 256 ){
                        System.out.println("Unexpected number of symbols in HMM <hmm:stream:symbols> : <"+ this.hmm_name + ":"+stream+":"+symbol_id);
                        break;
                    }

                }
            }

            //Next thing we read should be the transition matrix
            line = bufferedReader.readLine();
            space_tokenizer = new StringTokenizer(line);
            String this_token = (String)space_tokenizer.nextElement();
            if(this_token.equals("<TRANSP>") == false) {
                System.out.println("Expected transition matrix in HMM: "+this.hmm_name);
                break;
            }

            int states = Integer.parseInt((String)space_tokenizer.nextElement());
            if(this.num_states != states) {
                System.out.println("Unexpected number of states in in HMM: " + this.hmm_name + " expected "+ this.num_states + " read: " + states);
                break;
            }
            //Finally parse the transition function
            for(int from = 0; from < num_states; from++){
                line = bufferedReader.readLine();
                space_tokenizer = new StringTokenizer(line);
                if(space_tokenizer.countTokens() != num_states){
                    System.out.println("Unexpected number of states in trans fun of HMM: " + this.hmm_name + " expected "+ this.num_states + " read: " + space_tokenizer.countTokens());
                    break;
                }
                for(int to = 0 ; to < num_states; to++){
                    this.trans_fun[from][to] = Double.parseDouble((String)space_tokenizer.nextElement());
                }
            }

            //Now we must encounter the ENDHMM string
            line = bufferedReader.readLine();

            if(line.equals("<ENDHMM>") == false) {
                System.out.println("Unexpected end of HMM enountered in "+this.hmm_name);
            }

            ret_val = true;
        }while (false);

        bufferedReader.close();
        return ret_val;
    }/* hmm_parser */

}
