(defun read-four-bytes-be (in)
  (+  
    (+ (* (read-byte in) 16777216) (* (read-byte in) 65536) )
    (+ (* (read-byte in) 256) (read-byte in) )
  )
)

(defun read-two-bytes-be (in)
  (+ (* (read-byte in) 256) (read-byte in) )
)

(defparameter trace-htk-headers nil)
(defparameter *num-samples* 0)
(defparameter *sample-size-100ns* 0)
(defparameter *sample-bytes* 0)
(defparameter *sample-kind* 0)
(defparameter *htk-num-dims* 4)
(defun read-htk-header (data-stream)
  ;(format t "~&Reading speech vector with" )
  (setf *num-samples* (read-four-bytes-be data-stream))
  (if trace-htk-headers (format t "~& samples : ~S" *num-samples*) )
  (setf *sample-size-100ns* (read-four-bytes-be data-stream))
  (if trace-htk-headers (format t "~& sample period  : ~S us" (/ *sample-size-100ns* 10) ) )
  (setf *sample-bytes* (read-two-bytes-be  data-stream))
  (if trace-htk-headers (format t "~& sample width : ~S bytes per sample" *sample-bytes*) )
  (setf *sample-kind* (read-two-bytes-be  data-stream))
  (if trace-htk-headers (format t "~& sample type : ~S " *sample-kind*) )
)

;Reads one htk file and returns it in a list containing a list of observations
(defun read_htk_discrete_mfcc_vector (mfcd_file)
  (let ( 
        (mfcd_hdl (when mfcd_file (open mfcd_file :direction :input :if-does-not-exist :error :element-type 'unsigned-byte)) )
        (input_vector nil))
  (when mfcd_hdl 

    (read-htk-header mfcd_hdl) ;should reset num-samples and sample-bytes fields

    (if (not (equal 8 *sample-bytes* )) (format t "Worng htk file header for file ~S ~& " mfcd_file))
 
    (setq input_vector 
      (loop for samp from 1 to *num-samples*
        collect (loop for stream from 1 to *htk-num-dims* collect (read-two-bytes-be mfcd_hdl) ) ) )

    (close mfcd_hdl)
  )
  input_vector
  )
)
(defun get_test_list (file_hdl)
  (when file_hdl 
    (loop for line = (read-line file_hdl nil)
                        while line collect line) )
)
;returns a list of all audio samples in a list:
;  ( 
;    ( file_x
;      ( obs_dim1_t0 obs_dim2_t0 obs_dim3_t0 .. )  
;           ...
;      ( obs_dim1_tn obs_dim2_tn obs_dim3_tn .. )  
;    )
;     ...
;    ( file_M ().. () )
;  )

;(defparameter *global_test_list* '())
(defun read_all_htk_audio_files (file_name)
  (let ( (test_data '()))
    
   (with-open-file (my-file file_name :direction :input :if-does-not-exist :error) 
     (setq *global_test_list* (get_test_list my-file) )
    )
    ;When test_list is not nil 
    (when *global_test_list* 
      (setq test_data (loop for mfcd_file in *global_test_list*
                        collect (read_htk_discrete_mfcc_vector (concatenate 'string *htk_data_path* "/timit_data/" mfcd_file)) ) )
    )
    test_data
  
  )
)
