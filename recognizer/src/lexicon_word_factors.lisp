
;The phonetic lexicon is specified as phones and their positions in the words 
;  (1 word phone phone_posn) example: (1 dome d p0)
(defparameter *global_lexicon* '(
  ;(ax) (f uw) (y ih er z) (l ey dx er) (dh ih cl) (d ow m) (f eh l) (ih n) sil                               
  ;(0.005 * * *)
  (0.0 * * *)
  (1 a ax p0)
  (1 dome d p0) (1 dome ow p1) (1 dome m p2)
  (1 fell f p0) (1 fell eh p1) (1 fell l p2)
  (1 few f p0) (1 few y p1) (1 few uw p2) ;(1 few y p1)
  (1 in ih p0) (1 in ix p0) (1 in n p1)
 
  (1 later l p0) (1 later ey p1) (1 later dx p2) (1 later er p3) 
  
  (1 the dh p0) (1 the ih p1) (1 the ix p1)  (1 the cl p2) (1 the vcl p2)

  (1 years y p0) (1 years ih p1) (1 years er p2) (1 years z p3) 

  ;Add silence anywhere in the words
  ;(1 * sil *) ; now, observing silence anywhere in any word has no impact
  (0.0 <s> * *) 
  (0.99 <s> sil *)

  (0.01 a sil p0)
  (0.01 dome sil p0) (0.01 dome sil p1) (0.01 dome sil p2)
  (0.01 fell sil p0) (0.01 fell sil p1) (0.01 fell sil p2)
  (0.01 few sil p0) (0.01 few sil p1)
  (0.01 in sil p0) (0.01 in sil p1)
  (0.01 later sil p0) (0.01 later sil p1) (0.01 later sil p2) (0.01 later sil p3) 
  (0.01 the sil p0) (0.01 the sil p1) (0.01 the sil p2)
  (0.01 years sil p0) (0.01 years sil p1) (0.01 years sil p2) (0.01 years sil p3) 


))

(defparameter *global_lexicon_mappings* '(
  (0 * *)
  (1 ix ih) (1 ih ih)
  (1 ao aa) (1 aa aa)
  (1 el l)  (1 l l)
  (1 vcl cl) (1 cl cl)
  (1 epi cl)
  (1 ah ax) (1 ah ah)
  (1 zh sh) (1 sh sh)

))
(defparameter *global_lexicon_endings* '(
  (0 * * *)
  (1 a ax p0)
  (1 dome m p2)
  (1 fell l p2)
  (1 few uw p1)
  (1 in n p1)
  (1 later er p3) 
  (1 the cl p2)
  (1 the vcl p2)

  (1 years z p3) 

  ;Add silence anywhere in the words
  ;(1 * sil *) ; now, observing silence anywhere in any word has no impact
  (1 <s> sil )

  (0.01 * sil *) 
))


(defparameter *global_word_to_word_trans_fn* '(
  (0 * * * *)
  (0.9 a p0 a p0)
  (0.0 a p0 dome p0) (0 a p0 fell p0) (0.1 a p0 few p0) (0 a p0 in p0) (0 a p0 later p0) (0 a p0 the p0) (0 a p0 years p0) 

  (0.9 dome p0 dome p0) (0.1 dome p0 dome p1)(0.9 dome p1 dome p1) (0.1 dome p1 dome p2) (0.9 dome p2 dome p2)
  (0 dome p2 a p0) (0 dome p2 dome p0) (0.1 dome p2 fell p0) (0 dome p2 few p0) (0 dome p2 in p0) (0 dome p2 later p0) (0 dome p2 the p0) (0 dome p2 years p0) 
  
  (0.9 fell p0 fell p0) (0.1 fell p0 fell p1) (0.9 fell p1 fell p1) (0.1 fell p1 fell p2) (0.9 fell p2 fell p2)
  (0 fell p2 a p0) (0 fell p2 dome p0) (0 fell p2 fell p0) (0 fell p2 few p0) (0.1 fell p2 in p0) (0 fell p2 later p0) (0 fell p2 the p0) (0 fell p2 years p0) 
  
  (0.9 few p0 few p0) (0.1 few p0 few p1) (0.9 few p1 few p1) (0.1 few p1 few p2)
  (0 few p2 a p0) (0 few p2 dome p0) (0 few p2 fell p0) (0 few p2 few p0) (0 few p2 in p0) (0 few p2 later p0) (0 few p2 the p0) (0.1 few p2 years p0) 
  ;(0 few p1 a p0) (0 few p1 dome p0) (0 few p1 fell p0) (0 few p1 few p0) (0 few p1 in p0) (0 few p1 later p0) (0 few p1 the p0) (0.1 few p1 years p0) 
  
  (0.9 in p0 in p0) (0.1 in p0 in p1) (1.0 in p1 in p1)
  (0 in p1 a p0) (0 in p1 dome p0) (0 in p1 fell p0) (0 in p1 few p0) (0 in p1 in p0) (0 in p1 later p0) (0.0 in p1 the p0) (0 in p1 years p0) 
  
  (0.9 later p0 later p0) (0.1 later p0 later p1) (0.9 later p1 later p1) (0.1 later p1 later p2) (0.9 later p2 later p2) (0.1 later p2 later p3) (0.9 later p3 later p3)
  (0 later p3 a p0) (0 later p3 dome p0) (0 later p3 fell p0) (0 later p3 few p0) (0 later p3 in p0) (0 later p3 later p0) (0.1 later p3 the p0) (0 later p3 years p0) 
  
  (0.9 the p0 the p0) (0.1 the p0 the p1) (0.9 the p1 the p1) (0.1 the p1 the p2) (0.9 the p2 the p2)
  (0 the p2 a p0) (0.1 the p2 dome p0) (0 the p2 fell p0) (0 the p2 few p0) (0 the p2 in p0) (0 the p2 later p0) (0 the p2 the p0) (0 the p2 years p0) 
  
  (0.9 years p0 years p0) (0.1 years p0 years p1) (0.9 years p1 years p1) (0.1 years p1 years p2) (0.9 years p2 years p2) (0.1 years p2 years p3) (0.9 years p3 years p3)
  (0 years p3 a p0) (0 years p3 dome p0) (0 years p3 fell p0) (0 years p3 few p0) (0.0 years p3 in p0) (0.1 years p3 later p0) (0 years p3 the p0) (0 years p3 years p0) 


  (0.9 <s> p0 <s> p0)
  (0.1 <s> p0 a   p0) 
  
))

#|
(defparameter *global_word_to_word_trans_fn* '(
  (0 * * * *)
  (0.9 a p0 a p0)
  (0.0 a p0 dome p0) (0 a p0 fell p0) (0.1 a p0 few p0) (0 a p0 in p0) (0 a p0 later p0) (0 a p0 the p0) (0 a p0 years p0) 

  (0.9 dome p0 dome p0) (0.1 dome p0 dome p1)(0.9 dome p1 dome p1) (0.1 dome p1 dome p2) (0.9 dome p2 dome p2)
  (0 dome p2 a p0) (0 dome p2 dome p0) (1 dome p2 fell p0) (1 dome p2 few p0) (1 dome p2 in p0) (1 dome p2 later p0) (1 dome p2 the p0) (1 dome p2 years p0) 
  
  (1 fell p0 fell p0) (1 fell p0 fell p1) (1 fell p1 fell p1) (1 fell p1 fell p2) (1 fell p2 fell p2)
  (1 fell p2 a p0) (1 fell p2 dome p0) (1 fell p2 fell p0) (1 fell p2 few p0) (1 fell p2 in p0) (1 fell p2 later p0) (1 fell p2 the p0) (1 fell p2 years p0) 
  
  (1 few p0 few p0) (1 few p0 few p1) (1 few p1 few p1)
  (1 few p1 a p0) (1 few p1 dome p0) (1 few p1 fell p0) (1 few p1 few p0) (1 few p1 in p0) (1 few p1 later p0) (1 few p1 the p0) (1 few p1 years p0) 
  
  (1 in p0 in p0) (1 in p0 in p1) (1 in p1 in p1)
  (1 in p1 a p0) (1 in p1 dome p0) (1 in p1 fell p0) (1 in p1 few p0) (1 in p1 in p0) (1 in p1 later p0) (1 in p1 the p0) (1 in p1 years p0) 
  
  (1 later p0 later p0) (1 later p0 later p1) (1 later p1 later p1) (1 later p1 later p2) (1 later p2 later p2) (1 later p2 later p3) (1 later p3 later p3)
  (1 later p3 a p0) (1 later p3 dome p0) (1 later p3 fell p0) (1 later p3 few p0) (1 later p3 in p0) (1 later p3 later p0) (1 later p3 the p0) (1 later p3 years p0) 
  
  (1 the p0 the p0) (1 the p0 the p1) (1 the p1 the p1) (1 the p1 the p2) (1 the p2 the p2)
  (1 the p2 a p0) (1 the p2 dome p0) (1 the p2 fell p0) (1 the p2 few p0) (1 the p2 in p0) (1 the p2 later p0) (1 the p2 the p0) (1 the p2 years p0) 
  
  (1 years p0 years p0) (1 years p0 years p1) (1 years p1 years p1) (1 years p1 years p2) (1 years p2 years p2) (1 years p2 years p3) (1 years p3 years p3)
  (1 years p3 a p0) (1 years p3 dome p0) (1 years p3 fell p0) (1 years p3 few p0) (1 years p3 in p0) (1 years p3 later p0) (1 years p3 the p0) (1 years p3 years p0) 
  
))
|#
